#include "xplayvideo.h"
#include <QDebug>

void XPlayVideo::timerEvent(QTimerEvent *ev)
{
    if (player.is_pause())
    {
        ui.play->setStyleSheet(
            "background-image: url(:/XViewer/img/play.png);");
    }
    else
    {
        ui.play->setStyleSheet(
            "background-image: url(:/XViewer/img/pause.png);");
    }

    if (player.is_pause())
        return;
    player.Update();
    auto pos = player.pos_ms();
    auto total = player.total_ms();
    ui.pos->setMaximum(total);
    ui.pos->setValue(pos);

    // if (!view_)return;
    // auto f = decode_.GetFrame();
    // if (!f)return;
    // view_->DrawFrame(f);
    // XFreeFrame(&f);
}
void XPlayVideo::Close()
{
    player.Stop();
    ////关闭上次数据
    // demux_.Stop();
    // decode_.Stop();
    // if (view_)
    //{
    //     view_->Close();
    //     delete view_;
    //     view_ = nullptr;
    // }
}
void XPlayVideo::Pause()
{
    player.Pause(!player.is_pause());
}
void XPlayVideo::Move() // 进度条拖动
{
    player.Pause(true);
}
void XPlayVideo::PlayPos() // 控制播放进度
{
    player.Seek(ui.pos->value());
    player.Pause(false);
}
void XPlayVideo::SetSpeed()
{
    float speed = 1;
    int s = ui.speed->value();
    if (s <= 10)
    {
        speed = (float)s / (float)10;
    }
    else
    {
        speed = s - 9;
    }
    ui.speedtxt->setText(QString::number(speed));
    player.SetSpeed(speed);
}
void XPlayVideo::closeEvent(QCloseEvent *ev)
{
    Close();
}
bool XPlayVideo::Open(const char *url)
{
    if (!player.Open(url, (void *)ui.video->winId()))
        return false;
    player.Start();
    player.Pause(false); // 播放状态
    startTimer(10);

    // if (!demux_.Open(url)) //解封装
    //{
    //     return false;
    // }
    // auto vp = demux_.CopyVideoPara();
    // if (!vp)
    //     return false;
    // if (!decode_.Open(vp->para))//解码
    //{
    //     return false;
    // }
    // demux_.set_next(&decode_);
    //
    // if (!view_)
    //     view_ = XVideoView::Create();
    // view_->set_win_id((void*)winId());
    // if (!view_->Init(vp->para)) //SDL渲染
    //     return false;
    // demux_.set_syn_type(XSYN_VIDEO);
    // demux_.Start();
    // decode_.Start();
    return true;
}
XPlayVideo::XPlayVideo(QWidget *parent)
    : QDialog(parent)
{
    ui.setupUi(this);
}

XPlayVideo::~XPlayVideo()
{
    Close();
}
